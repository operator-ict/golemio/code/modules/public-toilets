import { SchemaDefinition } from "@golemio/core/dist/shared/mongoose";

// MSO = Mongoose SchemaObject

const datasourceMSO: SchemaDefinition = {
    geometry: {
        coordinates: { type: Array, required: true },
        type: { type: String, required: true },
    },
    properties: {
        ADRESA: { type: String },
        CENA: { type: String },
        OBJECTID: { type: Number, required: true },
        OTEVRENO: { type: String },
    },
    type: { type: String, required: true },
};

const outputMSO: SchemaDefinition = {
    geometry: {
        coordinates: { type: Array, required: true },
        type: { type: String, required: true },
    },
    properties: {
        address: {
            address_country: { type: String },
            address_formatted: { type: String },
            address_locality: { type: String },
            address_region: { type: String },
            postal_code: { type: String },
            street_address: { type: String },
        },
        district: { type: String },
        id: { type: Number, required: true },
        opened: { type: String },
        price: { type: String },
        updated_at: { type: Number, required: true },
    },
    type: { type: String, required: true },
};

const forExport = {
    datasourceMongooseSchemaObject: datasourceMSO,
    mongoCollectionName: "publictoilets",
    name: "PublicToilets",
    outputMongooseSchemaObject: outputMSO,
};

export { forExport as PublicToilets };
